import config from 'config';
import { authHeader, handleResponse } from '@/_helpers';
import { BehaviorSubject } from 'rxjs';

export const userService = {
    getAll
};

function getAll() {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
}